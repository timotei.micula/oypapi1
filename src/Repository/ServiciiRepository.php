<?php

namespace App\Repository;

use App\Entity\Servicii;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Servicii|null find($id, $lockMode = null, $lockVersion = null)
 * @method Servicii|null findOneBy(array $criteria, array $orderBy = null)
 * @method Servicii[]    findAll()
 * @method Servicii[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiciiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Servicii::class);
    }

    // /**
    //  * @return Servicii[] Returns an array of Servicii objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Servicii
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
