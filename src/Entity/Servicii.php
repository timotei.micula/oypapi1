<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ServiciiRepository")
 */
class Servicii
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language;

    /**
     * @ORM\Column(type="integer")
     */
    private $comercial;

    /**
     * @ORM\Column(type="integer")
     */
    private $tehnical;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getComercial(): ?int
    {
        return $this->comercial;
    }

    public function setComercial(int $comercial): self
    {
        $this->comercial = $comercial;

        return $this;
    }

    public function getTehnical(): ?int
    {
        return $this->tehnical;
    }

    public function setTehnical(int $tehnical): self
    {
        $this->tehnical = $tehnical;

        return $this;
    }
}
